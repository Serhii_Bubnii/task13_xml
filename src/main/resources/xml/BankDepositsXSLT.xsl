<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    table.tfmt {
                    border: 1px ;
                    }

                    td.colfmt {
                    border: 1px ;
                    background-color: #b6c0c9;
                    color: #000000;
                    text-align:right;
                    }

                    th {
                    background-color: #8797a6;
                    color: white;
                    }
                </style>
            </head>

            <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
                <div style="background-color: green; color: black;">
                    <h2>My bank deposits</h2>
                </div>
                <table class="tfmt">
                    <tr>
                        <th style="width:80px">deposit</th>
                        <th style="width:130px">Name</th>
                        <th style="width:100px">Country</th>
                        <th style="width:150px">Types</th>
                        <th style="width:100px">Depositor</th>
                        <th style="width:110px">AccountId</th>
                        <th style="width:150px">AmountOnDeposit</th>
                        <th style="width:100px">Profitability</th>
                        <th style="width:80px">Year</th>
                        <th style="width:80px">Month</th>
                        <th style="width:80px">Day</th>
                    </tr>

                    <xsl:for-each select="banks/bank">

                        <tr>
                            <td class="colfmt">
                                <xsl:value-of select="@deposit"/>
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="name"/>
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="country"/>
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="types"/>
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="depositor"/>
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="accountId"/>
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="amountOnDeposit"/>
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="profitability"/>
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="timeConstraints/year"/>
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="timeConstraints/month"/>
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="timeConstraints/day"/>
                            </td>
                        </tr>

                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>