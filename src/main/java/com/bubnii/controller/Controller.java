package com.bubnii.controller;

import com.bubnii.parser.Parser;

public class Controller {

    private Parser parser;

    public Controller() {
        this.parser = new Parser();
    }

    public void runParserDOM() {
        parser.parserDOM();
    }

    public void runParserSAX() {
        parser.parserSAX();
    }

    public void runParserStAX() {
        parser.parserStAX();
    }

}
