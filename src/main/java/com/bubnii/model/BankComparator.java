package com.bubnii.model;

import com.bubnii.model.entity.Bank;

import java.util.Comparator;

public class BankComparator implements Comparator<Bank> {

    @Override
    public int compare(Bank o1, Bank o2) {
        return Integer.compare(o1.getAmountOnDeposit(), o2.getAmountOnDeposit());
    }
}
