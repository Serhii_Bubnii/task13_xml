package com.bubnii.parser;

import com.bubnii.model.BankComparator;
import com.bubnii.model.ExtensionChecker;
import com.bubnii.model.entity.Bank;
import com.bubnii.parser.dom.DOMParserUser;
import com.bubnii.parser.sax.SAXParserUser;
import com.bubnii.parser.stax.StAXReader;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class Parser {
    private File xml = new File("c:\\Users\\bubni\\IdeaProjects\\task13_xml\\src\\main\\resources\\xml\\BankDepositsXML.xml");
    private File xsd = new File("c:\\Users\\bubni\\IdeaProjects\\task13_xml\\src\\main\\resources\\xml\\BankDepositsXSD.xsd");

    public void parserDOM() {
        if (checkIfXML(xml) && checkIfXSD(xsd)) {
//            DOM
            printList(DOMParserUser.getBankList(xml, xsd), "DOM");
        }
    }

    public void parserSAX() {
        if (checkIfXML(xml) && checkIfXSD(xsd)) {
//            SAX
            printList(SAXParserUser.parseBanks(xml, xsd), "SAX");
        }
    }

    public void parserStAX() {
        if (checkIfXML(xml) && checkIfXSD(xsd)) {
//            StAX
            printList(StAXReader.parseBanks(xml, xsd), "StAX");
        }
    }

    private static boolean checkIfXSD(File xsd) {
        return ExtensionChecker.isXSD(xsd);
    }

    private static boolean checkIfXML(File xml) {
        return ExtensionChecker.isXML(xml);
    }

    private static void printList(List<Bank> banks, String parserName) {
        Collections.sort(banks, new BankComparator());
        System.out.println(parserName);
        for (Bank bank : banks) {
            System.out.println(bank);
        }
    }
}
