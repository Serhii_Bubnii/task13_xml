package com.bubnii.parser.sax;

import com.bubnii.model.entity.Bank;
import com.bubnii.model.entity.Date;
import com.bubnii.model.entity.Type;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {

    private List<Bank> bankList = new ArrayList<>();
    private Bank bank = null;
    private List<Type> types = null;
    private Date date = null;


    private boolean bName = false;
    private boolean bCountry = false;
    private boolean bTypes = false;
    private boolean bType = false;
    private boolean bDepositor = false;
    private boolean bAccountId = false;
    private boolean bAmountOnDeposit = false;
    private boolean bProfitability = false;
    private boolean bTimeConstraints = false;
    private boolean bYear = false;
    private boolean bMonth = false;
    private boolean bDay = false;

    public List<Bank> getBankList() {
        return this.bankList;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("bank")) {
            String bankDeposit = attributes.getValue("deposit");
            bank = new Bank();
            bank.setDeposit(Integer.parseInt(bankDeposit));
        } else if (qName.equalsIgnoreCase("name")) {
            bName = true;
        } else if (qName.equalsIgnoreCase("country")) {
            bCountry = true;
        } else if (qName.equalsIgnoreCase("types")) {
            bTypes = true;
        } else if (qName.equalsIgnoreCase("type")) {
            bType = true;
        } else if (qName.equalsIgnoreCase("depositor")) {
            bDepositor = true;
        } else if (qName.equalsIgnoreCase("accountId")) {
            bAccountId = true;
        } else if (qName.equalsIgnoreCase("amountOnDeposit")) {
            bAmountOnDeposit = true;
        } else if (qName.equalsIgnoreCase("profitability")) {
            bProfitability = true;
        } else if (qName.equalsIgnoreCase("timeConstraints")) {
            bTimeConstraints = true;
        } else if (qName.equalsIgnoreCase("year")) {
            bYear = true;
        } else if (qName.equalsIgnoreCase("month")) {
            bMonth = true;
        } else if (qName.equalsIgnoreCase("day")) {
            bDay = true;
        }
    }

    public void endElement(String uri, String localName, String qName) {
        if (qName.equalsIgnoreCase("bank")) {
            bankList.add(bank);
        }
    }

    public void characters(char ch[], int start, int length) {
        if (bName) {
            bank.setName(new String(ch, start, length));
            bName = false;
        } else if (bCountry) {
            bank.setCountry(new String(ch, start, length));
            bCountry = false;
        } else if (bTypes) {
            types = new ArrayList<>();
            bTypes = false;
        } else if (bType) {
            Type type = new Type();
            type.setType(new String(ch, start, length));
            types.add(type);
            bType = false;
        } else if (bDepositor) {
            bank.setDepositor(new String(ch, start, length));
            bDepositor = false;
        } else if (bAccountId) {
            bank.setAccountId(Integer.parseInt(new String(ch, start, length)));
            bAccountId = false;
        } else if (bAmountOnDeposit) {
            bank.setAmountOnDeposit(Integer.parseInt(new String(ch, start, length)));
            bAmountOnDeposit = false;
        } else if (bProfitability) {
            bank.setProfitability(Double.parseDouble(new String(ch, start, length)));
            bProfitability = false;
        } else if (bTimeConstraints) {
            date = new Date();
            bTimeConstraints = false;
        } else if (bYear) {
            date.setYear(Integer.parseInt(new String(ch, start, length)));
            bYear = false;
        } else if (bMonth) {
            date.setMonth(new String(ch, start, length));
            bMonth = false;
        } else if (bDay) {
            date.setDay(new String(ch, start, length));
            bank.setDate(date);
            bank.setTypes(types);
            bDay = false;
        }
    }
}
