package com.bubnii.parser.stax;

import com.bubnii.model.entity.Bank;
import com.bubnii.model.entity.Date;
import com.bubnii.model.entity.Type;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StAXReader {

    public static List<Bank> parseBanks(File xml, File xsd) {
        List<Bank> bankList = new ArrayList<>();
        Bank bank = null;
        Date date = null;
        List<Type> typeList = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "name":
                            bank = new Bank();
                            xmlEvent = xmlEventReader.nextEvent();
                            assert bank != null;
                            bank.setName(xmlEvent.asCharacters().getData());
                            break;
                        case "country":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert bank != null;
                            bank.setCountry(xmlEvent.asCharacters().getData());
                            break;
                        case "types":
                            xmlEvent = xmlEventReader.nextEvent();
                            typeList = new ArrayList<>();
                            break;
                        case "type":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert typeList != null;
                            typeList.add(new Type(xmlEvent.asCharacters().getData()));
                            break;
                        case "depositor":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert bank != null;
                            bank.setDepositor(xmlEvent.asCharacters().getData());
                            break;
                        case "accountId":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert bank != null;
                            bank.setAccountId(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "amountOnDeposit":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert bank != null;
                            bank.setAmountOnDeposit(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "profitability":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert bank != null;
                            bank.setProfitability(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "timeConstraints":
                            xmlEvent = xmlEventReader.nextEvent();
                            date = new Date();
                            break;
                        case "year":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert date != null;
                            date.setYear(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "month":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert date != null;
                            date.setMonth(xmlEvent.asCharacters().getData());
                            break;
                        case "day":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert date != null;
                            date.setDay(xmlEvent.asCharacters().getData());
                            assert bank != null;
                            bank.setTypes(typeList);
                            bank.setDate(date);
                            break;
                    }
                }
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("bank")) {
                        bankList.add(bank);
                    }
                }
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return bankList;
    }
}
