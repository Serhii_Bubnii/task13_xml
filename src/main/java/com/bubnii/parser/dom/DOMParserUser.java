package com.bubnii.parser.dom;

import com.bubnii.model.entity.Bank;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class DOMParserUser {

    public static List<Bank> getBankList(File xml, File xsd) {
        DOMDocCreator domDocCreator = new DOMDocCreator(xml);
        Document doc = domDocCreator.getDocument();

//        DOMValidator.validate(DOMValidator.createSchema(xsd),doc);


        DOMDocReader docReader = new DOMDocReader();
        return docReader.readDoc(doc);
    }
}
