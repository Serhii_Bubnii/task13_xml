package com.bubnii.parser.dom;

import com.bubnii.model.entity.Bank;
import com.bubnii.model.entity.Date;
import com.bubnii.model.entity.Type;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMDocReader {

    public List<Bank> readDoc(Document doc) {
        doc.getDocumentElement().normalize();
        List<Bank> bankList = new ArrayList<>();
        NodeList nodeList = doc.getElementsByTagName("bank");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Bank bank = new Bank();
            List<Type> types;
            Date date;
            if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) nodeList.item(i);
                bank.setDeposit(Integer.parseInt(element.getAttribute("deposit")));
                bank.setName(element.getElementsByTagName("name").item(0).getTextContent());
                bank.setCountry(element.getElementsByTagName("country").item(0).getTextContent());
                types = getTypes(element.getElementsByTagName("types"));
                bank.setDepositor(element.getElementsByTagName("depositor").item(0).getTextContent());
                bank.setAccountId(Integer.parseInt(element.getElementsByTagName("accountId").item(0).getTextContent()));
                bank.setAmountOnDeposit(Integer.parseInt(element.getElementsByTagName("amountOnDeposit").item(0).getTextContent()));
                bank.setProfitability(Double.parseDouble(element.getElementsByTagName("profitability").item(0).getTextContent()));
                date = getDate(element.getElementsByTagName("timeConstraints"));
                bank.setDate(date);
                bank.setTypes(types);
            }
            bankList.add(bank);
        }
        return bankList;
    }

    private List<Type> getTypes(NodeList nodes) {
        List<Type> types = new ArrayList<>();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            NodeList nodeList = element.getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element el = (Element) node;
                    types.add(new Type(el.getTextContent()));
                }
            }
        }
        return types;
    }

    private Date getDate(NodeList timeConstraints) {
        Date date = new Date();
        if (timeConstraints.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) timeConstraints.item(0);
            date.setYear(Integer.parseInt(element.getElementsByTagName("year").item(0).getTextContent()));
            date.setMonth(element.getElementsByTagName("month").item(0).getTextContent());
            date.setDay(element.getElementsByTagName("day").item(0).getTextContent());
        }
        return date;
    }
}
